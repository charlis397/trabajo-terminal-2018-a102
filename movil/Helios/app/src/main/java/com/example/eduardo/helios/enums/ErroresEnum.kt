package com.example.eduardo.helios.enums

enum class ErroresEnum(val identificador:Int, val mensaje:String){
    TIPO_DATO_INCORRECTO(1,"Tipo de dato incorrecto."),CAMPO_VACIO(2,"Campo obligatorio.")
}