package com.example.eduardo.helios.modelo

import java.util.*


data class SensadoPanel(var id:Int = 0, var idPanel:Int = 0, var hora:Date = Date(), var voltaje:Double=0.0)