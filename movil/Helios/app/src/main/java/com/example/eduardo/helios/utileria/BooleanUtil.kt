package com.example.eduardo.helios.utileria

/**
 * Utileria a emplear para valores Booleanos
 */
class BooleanUtil {

    /**
     * Funciones estaticos
     */
    companion object {
        /**
         * Funcion que permite cambiar un valor Int a Boolean
         */
         fun toBoolean(valor:Int):Boolean{
             return valor != Numeros.CERO.valor
         }
    }
}