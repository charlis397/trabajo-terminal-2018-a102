package com.example.eduardo.helios.enums

enum class OperacionesEnum(val identificador:Int){
    REGISTRAR(1), EDITAR(2), ELIMINAR(3), CONSULTAR(4), MONITOREAR(5), MONITOREO_VARIABLES(6), PRIORIDAD(7),CAPACIDAD_CARGA(8), ERROR(9)
}