package com.example.eduardo.helios.activities

import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.InputFilter
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.eduardo.helios.GestionarDispositivos
import com.example.eduardo.helios.R
import com.example.eduardo.helios.enums.ErroresEnum
import com.example.eduardo.helios.modelo.Bateria
import com.example.eduardo.helios.utileria.DecimalDigitsInputFilter
import com.example.eduardo.helios.utileria.Validaciones
import kotlinx.android.synthetic.main.activity_agregar_bateria.*
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader


class AgregarBateria : AppCompatActivity(), Response.Listener<JSONObject>, Response.ErrorListener {


    private var modelo: Bateria = Bateria()

    private val validaciones: Validaciones = Validaciones()

    private var ip = ""

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_agregar_bateria)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val fin = BufferedReader(
                InputStreamReader(openFileInput("ip.txt")))
        ip = fin.readLine()
        fin.close()

        campo_voltajeMaximo.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(3, 2, 100.0))
        campo_voltajeMinimo.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(3, 2, 100.0))
        campo_temperatura.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(3, 2, 100.0))
        campo_corriente.filters =  arrayOf<InputFilter>(DecimalDigitsInputFilter(3, 2, 100.0))

        btnAceptar.setOnClickListener {

            if(!validarCamposVacios() && !validarTipoDato()){
                modelo.nombre = campo_nombre.text.toString()
                modelo.voltajeMax = campo_voltajeMaximo.text.toString().toDouble()
                modelo.voltajeMin = campo_voltajeMinimo.text.toString().toDouble()
                modelo.numeroCeldas = campo_celdas.text.toString().toInt()
                modelo.temperatura = campo_temperatura.text.toString().toDouble()
                modelo.hasMemoria = campo_memoria.toString().toBoolean()
                modelo.corriente = campo_corriente.text.toString().toDouble()

                var memoria = 0

                if(modelo.hasMemoria == true){
                    memoria = 1
                }

                val url = "http://$ip/insertarBateria.php?nombre=${modelo.nombre}" +
                        "&voltajeMax=${modelo.voltajeMax.toString()}" +
                        "&voltajeMin=${modelo.voltajeMin.toString()}" +
                        "&numeroCeldas=${modelo.numeroCeldas.toString()}" +
                        "&temperatura=${modelo.temperatura}" +
                        "&memoria=$memoria" +
                        "&corriente=${modelo.corriente}"


                val jrq = JsonObjectRequest(Request.Method.GET,url,null,this,this)
                val rq = Volley.newRequestQueue(this)

                rq.add(jrq)


            }

        }
    }

    override fun onResponse(response: JSONObject?) {
        startActivity(Intent(this, GestionarDispositivos::class.java))
        Toast.makeText(this,"La batería se registró exitosamente", Toast.LENGTH_LONG).show()
    }

    override fun onErrorResponse(error: VolleyError?) {
        Toast.makeText(this,"Error al insertar la batería", Toast.LENGTH_LONG).show()
    }










    /**
     * Metodo que permite validar que el tipo de dato sea acorde al modelo de informacion
     */
    private fun validarTipoDato():Boolean{
        var hasErrors = false
        if(!validaciones.validarNombre(campo_nombre.text.toString())){
            campo_nombre.error=ErroresEnum.TIPO_DATO_INCORRECTO.mensaje
            hasErrors = true
        }
        return  hasErrors
    }

    /**
     * Metodo que permite validar que los campos obligatorios sean llenados
     */
    private fun validarCamposVacios(): Boolean {
        var hasErrors = false
       if(campo_nombre.text.toString().isEmpty()){
            campo_nombre.error = ErroresEnum.CAMPO_VACIO.mensaje
            hasErrors = true
        }
        if (campo_voltajeMaximo.text.toString().isEmpty()){
            campo_voltajeMaximo.error = ErroresEnum.CAMPO_VACIO.mensaje
            hasErrors = true
        }
        if (campo_voltajeMinimo.text.toString().isEmpty()){
            campo_voltajeMinimo.error = ErroresEnum.CAMPO_VACIO.mensaje
            hasErrors = true
        }
        if (campo_celdas.text.toString().isEmpty()){
            campo_celdas.error = ErroresEnum.CAMPO_VACIO.mensaje
            hasErrors = true
        }
        if (campo_temperatura.text.toString().isEmpty()){
            campo_temperatura.error = ErroresEnum.CAMPO_VACIO.mensaje
            hasErrors = true
        }

        return  hasErrors
    }




    /**
     * Metodo que permite hacer la animacion del boton al ser presionado
     */
    fun buttonEffect(button: View) {
        button.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    v.background.setColorFilter(-0x320000, PorterDuff.Mode.SRC_ATOP)
                    v.invalidate()
                }
                MotionEvent.ACTION_UP -> {
                    v.background.clearColorFilter()
                    v.invalidate()
                }
            }
            false
        }
    }
}
