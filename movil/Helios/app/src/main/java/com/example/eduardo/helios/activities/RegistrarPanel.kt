package com.example.eduardo.helios.activities

import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.InputFilter
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.eduardo.helios.GestionarDispositivos
import com.example.eduardo.helios.R
import com.example.eduardo.helios.enums.ErroresEnum
import com.example.eduardo.helios.modelo.Panel
import com.example.eduardo.helios.utileria.DecimalDigitsInputFilter
import com.example.eduardo.helios.utileria.Validaciones
import kotlinx.android.synthetic.main.activity_registrar_panel.*
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader

class RegistrarPanel : AppCompatActivity(), Response.Listener<JSONObject>, Response.ErrorListener {



    private var modelo: Panel = Panel()

    private val validaciones: Validaciones = Validaciones()

    private var ip = ""

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registrar_panel)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val fin = BufferedReader(
                InputStreamReader(openFileInput("ip.txt")))
        ip = fin.readLine()
        fin.close()

        campo_voltaje.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(3, 2, 100.0))
        campo_corriente.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(3, 2, 100.0))
        btnAceptar.setOnClickListener {
            if(!validarCamposVacios() && !validarTipoDato()){
                modelo.nombre = campo_nombre.text.toString()
                modelo.voltajeMax = campo_voltaje.text.toString().toDouble()
                modelo.corrienteMax = campo_corriente.text.toString().toDouble()


                val url = "http://$ip/insertarPanel.php?nombre=${modelo.nombre}" +
                        "&voltajeMax=${modelo.voltajeMax}" +
                        "&corriente=${modelo.corrienteMax}"

                val jrq = JsonObjectRequest(Request.Method.GET,url,null,this,this)
                val rq = Volley.newRequestQueue(this)

                rq.add(jrq)
            }

        }

    }

    /**
     * Metodo que permite validar que el tipo de dato sea acorde al modelo de informacion
     */
    private fun validarTipoDato():Boolean{
        var hasErrors = false
        if(!validaciones.validarNombre(campo_nombre.text.toString())){
            campo_nombre.error=ErroresEnum.TIPO_DATO_INCORRECTO.mensaje
            hasErrors = true
        }
        return  hasErrors
    }

    /**
     * Metodo que permite validar que los campos obligatorios sean llenados
     */
    private fun validarCamposVacios(): Boolean {
        var hasErrors = false
        if(campo_nombre.text.toString().isEmpty()){
            campo_nombre.error = ErroresEnum.CAMPO_VACIO.mensaje
            hasErrors = true
        }
        if (campo_voltaje.text.toString().isEmpty()){
            campo_voltaje.error = ErroresEnum.CAMPO_VACIO.mensaje
            hasErrors = true
        }
        if (campo_corriente.text.toString().isEmpty()){
            campo_corriente.error = ErroresEnum.CAMPO_VACIO.mensaje
            hasErrors = true
        }
        return  hasErrors
    }

    override fun onResponse(response: JSONObject?) {
        startActivity(Intent(this, GestionarDispositivos::class.java))
        Toast.makeText(this,"El panel se registró exitosamente", Toast.LENGTH_LONG).show()
    }

    override fun onErrorResponse(error: VolleyError?) {
        Toast.makeText(this,"Error al insertar el panel ${error}", Toast.LENGTH_LONG).show()
    }



    /**
     * Metodo que permite hacer la animacion del boton al ser presionado
     */
    fun buttonEffect(button: View) {
        button.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    v.background.setColorFilter(-0x2f0b6adf , PorterDuff.Mode.SRC_ATOP)
                    v.invalidate()
                }
                MotionEvent.ACTION_UP -> {
                    v.background.clearColorFilter()
                    v.invalidate()
                }
            }
            false
        }
    }
}
