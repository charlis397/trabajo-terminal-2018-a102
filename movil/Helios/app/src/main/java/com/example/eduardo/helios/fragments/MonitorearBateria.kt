package com.example.eduardo.helios.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.ArrayAdapter
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.eduardo.helios.R
import com.example.eduardo.helios.enums.GraficasEnum
import com.example.eduardo.helios.enums.OperacionesEnum
import com.example.eduardo.helios.modelo.Bateria
import com.example.eduardo.helios.modelo.SensadoBateria
import com.example.eduardo.helios.utileria.DateUtil
import com.example.eduardo.helios.utileria.Numeros
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import kotlinx.android.synthetic.main.fragment_monitorear_bateria.*
import kotlinx.android.synthetic.main.fragment_monitorear_bateria.view.*
import org.joda.time.DateTime
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.absoluteValue
import kotlin.math.roundToInt


class MonitorearBateria : Fragment(), Response.Listener<JSONObject>, Response.ErrorListener, SwipeRefreshLayout.OnRefreshListener {
    override fun onRefresh() {
        obtenerBaterias()
        obtenerValoresVariables()
        obtenerCapacidadCarga()
        tablaFocos.removeAllViews()
        refreshMonitorearBateria.isRefreshing = false
    }

    var ip = ""
    private var tipoGrafica = 0
    private var nombreBateria = ""
    private var nombreVariable = ""
    private var nombrePeriodo = ""

    private var fechaInicio = ""
    private var fechaFin = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_monitorear_bateria, container, false)

        val fin = BufferedReader(
                InputStreamReader(activity!!.openFileInput("ip.txt")))
        ip = fin.readLine()
        fin.close()

        rootView.refreshMonitorearBateria.setOnRefreshListener(this)
        obtenerBaterias()
        obtenerValoresVariables()
        obtenerCapacidadCarga()


        val adapter = ArrayAdapter.createFromResource(activity,
                R.array.CTG_PERIODO, android.R.layout.simple_spinner_item)
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item)

        val adapterVariable = ArrayAdapter.createFromResource(activity,
                R.array.CTG_VARIABLE, android.R.layout.simple_spinner_item)
        adapterVariable.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item)

        if (rootView.tipoPeriodo != null) {
            rootView.tipoPeriodo.adapter = adapter
            rootView.variable.adapter = adapterVariable
            rootView.tipoPeriodo?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{

                val now = DateTime.parse("2019-04-30")

                override fun onNothingSelected(parent: AdapterView<*>) {
                    nombrePeriodo = parent.selectedItem.toString()
                }

                override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                    tipoGrafica = position
                    nombrePeriodo = parent.getItemAtPosition(position).toString()

                    if (nombrePeriodo == "Dia") {
                        fechaInicio = "${now.year}-${now.monthOfYear}-${now.dayOfMonth}"
                        fechaFin = "${now.year}-${now.monthOfYear}-${now.dayOfMonth+1}"
                    } else if (nombrePeriodo == "Semana") {
                        fechaInicio = DateUtil.calcularFechaInicial()
                        fechaFin = DateUtil.calcularFechaFinal()
                    } else if (nombrePeriodo == "Mes") {
                        fechaInicio = "${now.year}-${now.monthOfYear}-1"
                        fechaFin = "${now.year}-${now.monthOfYear}-28"
                    }

                    val url = "http://$ip/monitoreoBateria.php?nombre=$nombreBateria" +
                            "&fechaInicio=$fechaInicio" +
                            "&fechaFin=$fechaFin"
                    val jrq = JsonObjectRequest(Request.Method.GET, url, null, this@MonitorearBateria, this@MonitorearBateria)
                    val rq = Volley.newRequestQueue(context)
                    rq.add(jrq)
                }

            }
        }

        if (rootView.variable != null) {
            rootView.variable.adapter = adapterVariable
            rootView.variable?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{

                override fun onNothingSelected(parent: AdapterView<*>) {
                    nombreVariable = parent.selectedItem.toString()
                }

                override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                    nombreVariable = parent.getItemAtPosition(position).toString()
                }

            }
        }

        if (rootView.bateriaSpinner != null) {
            rootView.bateriaSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{

                override fun onNothingSelected(parent: AdapterView<*>) {
                    nombreBateria = parent.selectedItem.toString()
                }

                override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                    nombreBateria = parent.getItemAtPosition(position).toString()
                    val url = "http://$ip/monitoreoBateria.php?nombre=$nombreBateria" +
                            "&fechaInicio=$fechaInicio" +
                            "&fechaFin=$fechaFin"
                    val jrq = JsonObjectRequest(Request.Method.GET, url, null, this@MonitorearBateria, this@MonitorearBateria)
                    val rq = Volley.newRequestQueue(context)
                    rq.add(jrq)
                }
            }
        }
        return rootView
    }

    override fun onResponse(response: JSONObject) {
        val listaBaterias: ArrayList<Bateria> = ArrayList()
        val nombreBaterias: ArrayList<String> = ArrayList()

        var voltajeActualSensado = 0.0

        val jsonArray: JSONArray = response.optJSONArray("datos")


        var longitud = jsonArray.length()

        var tipoRespuestaJson = jsonArray.getJSONObject(0)
        val tipoRespuesta = tipoRespuestaJson.optInt("operacion")



        if (tipoRespuesta == OperacionesEnum.CONSULTAR.identificador) {
            for (i in 1 until longitud) {

                var jsonObject = jsonArray.getJSONObject(i)

                var bateria: Bateria = Bateria()

                bateria.id = jsonObject.optInt("id_bateria")
                bateria.nombre = jsonObject.optString("nombre")
                bateria.voltajeMax = jsonObject.optDouble("voltaje_max")
                bateria.voltajeMin = jsonObject.optDouble("voltaje_min")
                bateria.corriente = jsonObject.optDouble("corriente")
                bateria.numeroCeldas = jsonObject.optInt("nu_celdas")
                bateria.temperatura = jsonObject.optDouble("temperatura_max")
                bateria.hasMemoria = jsonObject.optBoolean("hasMemoria")
                bateria.isEliminado = jsonObject.optBoolean("isEliminado")

                listaBaterias.add(bateria)
            }

            listaBaterias.forEach {
                nombreBaterias.add(it.nombre)
            }

            val adapter = ArrayAdapter(context,android.R.layout.simple_spinner_dropdown_item,nombreBaterias)


            bateriaSpinner.adapter = adapter


        }else if(tipoRespuesta == OperacionesEnum.MONITOREAR.identificador){


            val listaSensadoBateria: ArrayList<SensadoBateria> = ArrayList()
            for (i in 1 until longitud) {
                var jsonObject = jsonArray.getJSONObject(i)

                var sensadoBateria = SensadoBateria()
                sensadoBateria.id = jsonObject.optInt("id_sensado_b")
                sensadoBateria.idBateria = jsonObject.optInt("id_bateria")
                sensadoBateria.hora = SimpleDateFormat("yyyy-mm-dd HH:mm:ss").parse(jsonObject.optString("hora"))
                sensadoBateria.voltaje = jsonObject.optDouble("voltaje")
                sensadoBateria.corriente = jsonObject.optDouble("corriente")
                sensadoBateria.temperatura = jsonObject.optDouble("temperatura")

                listaSensadoBateria.add(sensadoBateria)
            }
            var entradas = ArrayList<Entry>()
            val etiquetas = ArrayList<String>()

            var label = ""

            listaSensadoBateria.forEach {

                if(tipoGrafica == GraficasEnum.DIA.identficiador) {
                    var hora = it.hora.hours
                    var minuto = it.hora.minutes
                    label = "${nombreVariable} por día de la batería ${nombreBateria}"
                    var tiempo = "$hora.$minuto"

                    if(nombreVariable == "Voltaje"){
                        entradas.add(Entry(tiempo.toFloat(), it.voltaje.toFloat()))
                    }else if(nombreVariable == "Corriente"){
                        entradas.add(Entry(tiempo.toFloat(), it.corriente.toFloat()))
                    }else if(nombreVariable == "Temperatura"){
                        entradas.add(Entry(tiempo.toFloat(), it.temperatura.toFloat()))
                    }

                    etiquetas.add(tiempo)
                }else if(tipoGrafica == GraficasEnum.SEMANA.identficiador){
                    label = "${nombreVariable} por semana de la batería ${nombreBateria}"
                    var calendar = Calendar.getInstance()
                    calendar.time = it.hora
                    entradas.add(Entry(calendar.get(Calendar.DAY_OF_MONTH).toFloat(), it.voltaje.toFloat()))
                    if(nombreVariable == "Voltaje"){
                        entradas.add(Entry(calendar.get(Calendar.DAY_OF_MONTH).toFloat(), it.voltaje.toFloat()))
                    }else if(nombreVariable == "Corriente"){
                        entradas.add(Entry(calendar.get(Calendar.DAY_OF_MONTH).toFloat(), it.corriente.toFloat()))
                    }else if(nombreVariable == "Temperatura"){
                        entradas.add(Entry(calendar.get(Calendar.DAY_OF_MONTH).toFloat(), it.temperatura.toFloat()))
                    }
                }else if(tipoGrafica == GraficasEnum.MES.identficiador){
                    label = "${nombreVariable} por mes de la batería ${nombreBateria}"
                    var calendar = Calendar.getInstance()
                    calendar.time = it.hora
                    entradas.add(Entry(calendar.get(Calendar.DAY_OF_MONTH).toFloat(), it.voltaje.toFloat()))
                    if(nombreVariable == "Voltaje"){
                        entradas.add(Entry(calendar.get(Calendar.DAY_OF_MONTH).toFloat(), it.voltaje.toFloat()))
                    }else if(nombreVariable == "Corriente"){
                        entradas.add(Entry(calendar.get(Calendar.DAY_OF_MONTH).toFloat(), it.corriente.toFloat()))
                    }else if(nombreVariable == "Temperatura"){
                        entradas.add(Entry(calendar.get(Calendar.DAY_OF_MONTH).toFloat(), it.temperatura.toFloat()))
                    }
                }
            }
            val datos = LineDataSet(entradas, label)
            datos.color = R.color.graficasColor
            val data = LineData(datos)
            grafica.data = data

            //grafica.xAxis.valueFormatter = IndexAxisValueFormatter(etiquetas)
            //grafica.xAxis.position = XAxis.XAxisPosition.BOTH_SIDED
            //grafica.setViewPortOffsets(0f, 0f, 0f, 0f)



        }else if(tipoRespuesta == OperacionesEnum.MONITOREO_VARIABLES.identificador){
            var jsonObject = jsonArray.getJSONObject(1)

            var sensadoBateria = SensadoBateria()

            sensadoBateria.id = jsonObject.optInt("id_sensado_b")
            sensadoBateria.idBateria = jsonObject.optInt("id_bateria")
            sensadoBateria.hora = SimpleDateFormat("yyyy-mm-dd HH:mm:ss").parse(jsonObject.optString("hora"))
            sensadoBateria.voltaje = jsonObject.optDouble("voltaje")
            sensadoBateria.corriente = jsonObject.optDouble("corriente")
            sensadoBateria.temperatura = jsonObject.optDouble("temperatura")
            sensadoBateria.voltajePWM = jsonObject.optDouble("voltaje_pwm")
            sensadoBateria.estadoFase = jsonObject.optInt("id_estado_fase")

            voltajeActualSensado = sensadoBateria.voltaje


            energiaText.text = "${sensadoBateria.voltaje.toString()} V"
            termometroText.text = "${sensadoBateria.temperatura.toString()}°C"


        }else if(tipoRespuesta == OperacionesEnum.CAPACIDAD_CARGA.identificador){

            var jsonObject = jsonArray.getJSONObject(Numeros.UNO.valor)

            var bateria = Bateria()

            bateria.id = jsonObject.optInt("id_bateria")
            bateria.nombre = jsonObject.optString("nombre")
            bateria.voltajeMax = jsonObject.optDouble("voltaje_max")
            bateria.voltajeMin = jsonObject.optDouble("voltaje_min")
            bateria.corriente = jsonObject.optDouble("corriente")

            var horas = calcularHoras(200.0,bateria.corriente!!)
            var numeroFocos = calcularNumeroFocos(100.0,bateria.corriente!!)


            val porcentaje = calcularPorcentajeBateria(voltajeActualSensado,bateria?.voltajeMin!!,bateria.voltajeMax!!)

            bateriaText.text = "${porcentaje.toString()} %"
            horasFoco.text = Html.fromHtml("Una unidad luminosa puede ser alimentada durante <b>${horas} hora(s) </b>  con la batería actual.")
            pintarFocos(numeroFocos)
        }
    }

    override fun onErrorResponse(error: VolleyError?) {
        Toast.makeText(activity,error.toString(), Toast.LENGTH_SHORT).show()
    }


    /**
     * Funcion que realiza petion HTTP para solicitar los valores del ultimo senso a la bateria
     */
    private fun obtenerValoresVariables(){
        val url = "http://$ip/ultimoRegistroBateria.php"
        val jrq = JsonObjectRequest(Request.Method.GET,url,null,this,this)
        val rq = Volley.newRequestQueue(context)
        rq.add(jrq)
    }

    /**
     * Funcion que realiza peticion HTTP para obtener todas las baterias registradas y borradas
     */
    private fun obtenerBaterias(){
        val url = "http://$ip/consultarHistorialBateria.php"
        val jrq = JsonObjectRequest(Request.Method.GET,url,null,this@MonitorearBateria,this@MonitorearBateria)
        val rq = Volley.newRequestQueue(context)
        rq.add(jrq)
    }



    /**
     * Funcion que realizar peticion HTTP para solicitar la capacidad de carga que tiene la bateria actualmente empleada.
     */
    private fun obtenerCapacidadCarga(){
        val url = "http://$ip/capacidadCarga.php"
        val jrq = JsonObjectRequest(Request.Method.GET,url,null,this,this)
        val rq = Volley.newRequestQueue(context)
        rq.add(jrq)
    }

    /**
     * Permite calcular el numero de renglones que tendra la tabla que muestra los focos
     */
    private fun calcularRows(numeroFocos:Int): Int{
        var rows = 0

        if(numeroFocos <= 8){
            rows = 1
        }else if(numeroFocos.rem(8) == 0 ){
            rows = numeroFocos / 8
        }else{
            rows = (numeroFocos/8).absoluteValue + 1
        }
        return rows
    }


    /**
     * Mertodo que permite pintar los focos
     */

    private fun pintarFocos(numeroFocos: Int){
        var listTableRow :ArrayList<TableRow> = ArrayList()
        val rows = calcularRows(numeroFocos)

        val tableRowParams = TableLayout.LayoutParams()
        tableRowParams.setMargins(0, 4, 0, 4)


        if(numeroFocos <= 8 || numeroFocos.rem(8) == 0 ){
            for (i in 0 until rows) {
                var tableRow = TableRow(context)
                for(j in 0 until numeroFocos){
                    val foco = ImageView(context)
                    foco.setImageResource(R.drawable.ic_foco)
                    tableRow.layoutParams = tableRowParams
                    tableRow.addView(foco)

                }
                listTableRow.add(tableRow)
            }
        }else{
            for (i in 0 until rows-1) {
                var tableRow = TableRow(context)
                for(j in 0 until 8){
                    val foco = ImageView(context)
                    foco.setImageResource(R.drawable.ic_foco)
                    tableRow.layoutParams = tableRowParams
                    tableRow.addView(foco)

                }
                listTableRow.add(tableRow)
            }
            for (k in 0 until 1) {
                var tableRow = TableRow(context)
                for(l in 0 until numeroFocos.rem(8)){
                    val foco = ImageView(context)
                    tableRow.layoutParams = tableRowParams
                    foco.setImageResource(R.drawable.ic_foco)
                    tableRow.addView(foco)

                }
                listTableRow.add(tableRow)
            }
        }

        listTableRow.forEach {

            tablaFocos.addView(it)
        }
    }

    /**
     * Funcion que permite calcular el numero de focos que se pueden alimentar por hora cuando la bateria tenga su carga completa
     */
    private fun calcularNumeroFocos(corrienteFocos:Double, corrienteBateria: Double):Int{
        return corrienteBateria.div(corrienteFocos).roundToInt()
    }

    /**
     * Funcion que permite calcular el numero de focos que se pueden alimentar por hora cuando la bateria tenga su carga completa
     */
    private fun calcularHoras(corrienteFocos:Double, corrienteBateria: Double):Double{
        return corrienteBateria.div(corrienteFocos)
    }

    private fun calcularPorcentajeBateria(voltajeMax:Double, voltajeMin:Double, voltajeActual:Double):Double{
        return ((100/(voltajeMax.minus(voltajeMin)) * voltajeActual) +100) - ((100/voltajeMax.minus(voltajeMin))* voltajeMax)
    }




    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        fun newInstance(): MonitorearBateria {
            return MonitorearBateria()
        }
    }
}
