package com.example.eduardo.helios.enums

enum class GraficasEnum(val identficiador:Int){
    DIA(0), SEMANA(1), MES(2)
}