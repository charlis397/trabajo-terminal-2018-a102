package com.example.eduardo.helios.utileria

import org.joda.time.DateTime


/**
 * Utileria a emplear para valores Booleanos
 */
class DateUtil {

    /**
     * Funciones estaticos
     */
    companion object {
        /**
         * Funcion que permite calcular la fecha inicio para el tipo Semanal
         */
         fun calcularFechaInicial(): String{
            val now = DateTime.parse("2019-04-30")
            val firstDay = now.minusDays(now.dayOfWeek-1)
            val fecha = "${firstDay.year}-${firstDay.monthOfYear}-${firstDay.dayOfMonth}"
            return fecha
        }

        /**
         * Funcion que permite calcular la fecha fin para el tipo Semanal
         */
         fun calcularFechaFinal(): String{
            val now = DateTime.parse("2019-04-30")
            val ultimateDay = now.plusDays(7-now.dayOfWeek)
            val fecha = "${ultimateDay.year}-${ultimateDay.monthOfYear}-${ultimateDay.dayOfMonth}"
            return fecha
        }
    }
}