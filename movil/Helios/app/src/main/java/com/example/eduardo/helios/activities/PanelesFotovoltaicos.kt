package com.example.eduardo.helios.activities


import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.eduardo.helios.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_paneles_fotovoltaicos.*
import java.io.BufferedReader
import java.io.InputStream


class PanelesFotovoltaicos : AppCompatActivity() {

    override fun onBackPressed() {
        super.onBackPressed()
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_paneles_fotovoltaicos)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        funcionamientoTV.text = leerArchivo(1).first()
        instalacionTV.text = leerArchivo(2).first()
        orientacionTV.text = leerArchivo(2).last()

        cargarImagenes()



    }

    /**
     * Metodo que permite leer el archivo Funcionamiento almacenado en el la carpeta raw
     */
    private fun leerArchivo(numeroArchivo:Int):List<String>{
        var ins: InputStream = resources.openRawResource(R.raw.fucionamiento)
        if(numeroArchivo == 2){
            ins= resources.openRawResource(R.raw.orientacion)
        }
        val br: BufferedReader = ins.bufferedReader()
        return br.readLines()
    }



    /**
     * Metodo que permite Cargar las imagenes de este activity
     */
    private fun cargarImagenes() {

        Picasso.with(this).load("https://www.mpptsolar.com/es/imagenes/guias/inclinacion-paneles/orientacion-inclinacion-paneles-solares.jpg")
                .fit()
                .centerInside()
                .into(instalacionIV)

        Picasso.with(this).load("http://www.sunplicity.cl/wp-content/uploads/2016/11/paneles-solares-1170x780.jpg")
                .fit()
                .centerInside()
                .into(funcionamientoIV)


    }





}
