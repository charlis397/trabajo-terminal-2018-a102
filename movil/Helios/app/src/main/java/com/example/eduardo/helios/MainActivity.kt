package com.example.eduardo.helios

import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Patterns
import android.view.MotionEvent
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.eduardo.helios.activities.OrientacionOptima
import com.example.eduardo.helios.activities.PanelesFotovoltaicos
import com.example.eduardo.helios.enums.ErroresEnum
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.io.OutputStreamWriter


class  MainActivity : AppCompatActivity(), Response.Listener<JSONObject>, Response.ErrorListener {

    private var opcion = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        setContentView(R.layout.activity_main)


        fun irDispositivos(){
            opcion = 1
            mostrarDialogoIP()
        }
        fun irPaneles() = startActivity(Intent(this, PanelesFotovoltaicos::class.java))
        fun irOrientacion() = startActivity(Intent(this, OrientacionOptima::class.java))
        fun irMonitoreo() {
            opcion = 2
            mostrarDialogoIP()
        }


        btnCalculadora.setOnClickListener {  irOrientacion()}
        btnDispositivo.setOnClickListener { irDispositivos() }
        btnPaneles.setOnClickListener { irPaneles() }
        btnMonitoreo.setOnClickListener { irMonitoreo() }

        efectoPresion(btnCalculadora)
        efectoPresion(btnDispositivo)
        efectoPresion(btnPaneles)


    }

    fun mostrarDialogoIP() {
        val builder = AlertDialog.Builder(this)
        val inflater = layoutInflater
        builder.setTitle("Dirección IP")
        val dialogLayout = inflater.inflate(R.layout.layout_ip_dialog, null)
        val editText  = dialogLayout.findViewById<EditText>(R.id.campo_ip)
        builder.setView(dialogLayout)
        builder.setPositiveButton("Aceptar") { dialogInterface, i ->

            val matcher = Patterns.IP_ADDRESS.matcher(editText.text)

            if (matcher.matches()) {
                val osw = OutputStreamWriter(
                        openFileOutput("ip.txt", Context.MODE_PRIVATE))
                osw.write(editText.text.toString())
                osw.close()

                val url = "http://${editText.text}/panelDAO.php"
                val jrq = JsonObjectRequest(Request.Method.GET, url, null, this, this)
                val rq = Volley.newRequestQueue(applicationContext)
                rq.add(jrq)
            }
            else{
                Toast.makeText(this,ErroresEnum.TIPO_DATO_INCORRECTO.mensaje, Toast.LENGTH_SHORT).show()
            }
        }
        builder.show()
    }


    override fun onResponse(response: JSONObject?) {
        if(opcion == 1) startActivity(Intent(this, GestionarDispositivos::class.java))
        else startActivity(Intent(this, MonitorearMecanismo::class.java))
        Toast.makeText(this,"Conexión exitosa", Toast.LENGTH_LONG).show()
    }

    override fun onErrorResponse(error: VolleyError?) {
        Toast.makeText(this,"Error de conexión, verifique su IP ", Toast.LENGTH_SHORT).show()
    }

    /**
     * Metodo que permite hacer la animacion del boton al ser presionado
     */
    fun efectoPresion(button: View) {
        button.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    v.background.setColorFilter(-0x0d0f05 , PorterDuff.Mode.SRC_ATOP)
                    v.invalidate()
                }
                MotionEvent.ACTION_UP -> {
                    v.background.clearColorFilter()
                    v.invalidate()
                }
            }
            false
        }
    }

    //^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$
}
