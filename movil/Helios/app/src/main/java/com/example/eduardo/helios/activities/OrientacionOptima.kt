package com.example.eduardo.helios.activities

import android.app.DatePickerDialog
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.example.eduardo.helios.R
import com.example.eduardo.helios.enums.ErroresEnum
import com.example.eduardo.helios.enums.EstacionesEnum
import com.example.eduardo.helios.enums.PuntosCardinalesEnum
import com.example.eduardo.helios.modelo.Orientacion
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_orientacion_optima.*
import org.joda.time.DateTime
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.absoluteValue


class OrientacionOptima : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnMarkerDragListener {

    /**
     * Instancia de FusedLocationProviderClient
     */
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    /**
     * Ultima ubicacion del usuario
     */
    private lateinit var  lastLocation: Location
    /**
     * Contiene la ip
     */
    var ip = ""

    /**
     * Contiene la ubicacion seleccionada
     */
    var ubicacionSeleccionada: Address ?=null

    /**
     * Fecha seleccionada
     */
    var fechaSeleccionada = DateTime.now()

    companion object {

        /**
         *  Determina que 1 es que los permisos de localizacion estan activados
         */
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }

    /**
     * El control del marcador del mapa se asigna a la clase
     */
    override fun onMarkerClick(p0: Marker?) = false

    override fun onBackPressed() {
        super.onBackPressed()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_orientacion_optima)
        //supportActionBar?.setDisplayHomeAsUpEnabled(true)


        val btnCerrar = btnCerrarMapa
        val fechaTxt: TextView  = findViewById(R.id.fechaOrientacion)
        val ubicacionTxt: TextView = findViewById(R.id.maps)

        val map = supportFragmentManager
                .findFragmentById(R.id.mapF) as SupportMapFragment

        val fragmentT = supportFragmentManager.beginTransaction()
        fragmentT.hide(map)
        fragmentT.commit()


        var cal = Calendar.getInstance()
        val dateSetListener = cargarCalendario()



        fechaTxt.setOnClickListener {
            fechaTxt.inputType = InputType.TYPE_NULL
            DatePickerDialog(this@OrientacionOptima, dateSetListener,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()
        }

        ubicacionTxt.setOnClickListener {
            ubicacionTxt.inputType = InputType.TYPE_NULL
            btnCerrar.visibility = View.VISIBLE
            btnCalcular.visibility = View.INVISIBLE
            val mapFragment = supportFragmentManager
                    .findFragmentById(R.id.mapF) as SupportMapFragment
            mapFragment.getMapAsync(this)
            val fragmentManager = supportFragmentManager.beginTransaction()
            fragmentManager.show(mapFragment).commit()


            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        }

        btnCerrar.setOnClickListener {
            btnCerrar.visibility = View.INVISIBLE
            btnCalcular.visibility = View.VISIBLE
            supportFragmentManager.beginTransaction().hide(map).commit()
        }

        btnCalcular.setOnClickListener {

            if (!validarCamposVacios()) {

                val builder = AlertDialog.Builder(this)
                val inflater = layoutInflater

                val dialogLayout = inflater.inflate(R.layout.orientacion_optima_dialogo, null)


                val orientacion = calcularOrientacion(this.ubicacionSeleccionada!!)


                val textView = dialogLayout.findViewById<TextView>(R.id.leyendaOrientacion)
                val grados = dialogLayout.findViewById<TextView>(R.id.grados)
                textView.text = "La orientación ideal de un panel fotovoltaico " +
                        "en ${ubicacionSeleccionada?.adminArea} para el día ${SimpleDateFormat("dd-MMMM-yyyy").format(fechaSeleccionada.toDate())} es:"



                grados.text = "${orientacion.grados}° al ${orientacion.puntoCardinal}"
                builder.setView(dialogLayout)

                builder.show()
            }
        }


    }

    /**
     * Metodo que determina la orientacion ideal que debe tener un panel fotovoltaico apartir de su ubicacion y fecha seleccionada
     *
     * En sentido estricto solo interesan dos estaciones del año, Invierno y Verano ya que de estas se calcula el grado de inclinacion
     *
     * Si se encuentra en el hemisferio sur, se orienta al norte.
     *
     * Si se encuentra en el hemisferio norte, se orienta al sur
     */
    private fun calcularOrientacion(ubicacion: Address): Orientacion {

        val fechaJunio = DateTime("${fechaSeleccionada.year}-06-20") // 21 de Junio
        val fechaDiciembre = DateTime("${fechaSeleccionada.year}-12-21") // 21 de Diciembre
        var gradoInclinacion = 0

        val orientacion = Orientacion()

        if (ubicacion.latitude < 0) {
            //HEMISFERIO SUR
            orientacion.puntoCardinal = PuntosCardinalesEnum.NORTE.nombre

            if(fechaSeleccionada.isAfter(fechaJunio) && fechaSeleccionada.isBefore(fechaDiciembre)){
                orientacion.estacion = EstacionesEnum.INVIERNO.nombre
                orientacion.grados = obtenerGradoInclinacion( EstacionesEnum.INVIERNO.identificador,ubicacion.latitude)
            }
            else {
                orientacion.estacion = EstacionesEnum.VERANO.nombre
                orientacion.grados = obtenerGradoInclinacion( EstacionesEnum.VERANO.identificador,ubicacion.latitude)
            }
            return orientacion

        } else if (ubicacion.latitude > 0) {
            //HEMISFERIO NORTE

            orientacion.puntoCardinal = PuntosCardinalesEnum.SUR.nombre

            if(fechaSeleccionada.isAfter(fechaJunio) && fechaSeleccionada.isBefore(fechaDiciembre)){
                orientacion.estacion = EstacionesEnum.VERANO.nombre
                orientacion.grados = obtenerGradoInclinacion( EstacionesEnum.VERANO.identificador,ubicacion.latitude)
            }
            else {
                orientacion.estacion = EstacionesEnum.INVIERNO.nombre
                orientacion.grados = obtenerGradoInclinacion( EstacionesEnum.INVIERNO.identificador,ubicacion.latitude)
            }

        }
        return orientacion
    }

    /**
     * Obtiene el grado de inclinacion del panel, dado la estacion (VERANO o INVIERNO) y la latitud.
     *
     * 23 es la inclinacion de la tierra
     *
     */
    private fun obtenerGradoInclinacion(estacion:Int, latitud: Double): Int{

        val inclinacionTerrestre = 23

        return if(estacion == (EstacionesEnum.VERANO.identificador)){
            90-(latitud-inclinacionTerrestre).absoluteValue.toInt()
        }else{
            90-(latitud+inclinacionTerrestre).absoluteValue.toInt()
        }
    }



    /**
     * Metodo que carga el mapa correspondiente al campo ubicacion
     */
    override fun onMapReady(map: GoogleMap) {
        setUpMap()

        map.isMyLocationEnabled = true
        fusedLocationClient.lastLocation.addOnSuccessListener(this) {  location ->

            if(location != null){
                lastLocation = location
                val currentLatLong = LatLng(location.latitude, location.longitude)
                placeMarker(currentLatLong,map)
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLong,5f))
            }
        }
        map.setOnMarkerClickListener(this)
        map.setOnMarkerDragListener(this)
        map.uiSettings.isZoomControlsEnabled = true
        map.uiSettings.isRotateGesturesEnabled = false
    }


    /**
     * Metodo que determina si los permisos de ubicacion estan habilitados, si no es asi los solicita
     */
    private fun setUpMap(){
        if(ActivityCompat.checkSelfPermission(this,android.Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            return
        }
    }

    /**
     * Metodo que permite colocar un marcador en el mapa
     */
    private fun placeMarker(location: LatLng, map:GoogleMap){
        var markerOption = MarkerOptions().position(location)
        map.clear()
        markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
        markerOption.draggable(true)
        map.addMarker(markerOption)
        var geocoder = Geocoder(this)
        var ubicacion = geocoder.getFromLocation(markerOption.position.latitude,markerOption.position.longitude,1)
        maps.setText("${ubicacion.first().countryName}, ${ubicacion.first().adminArea}")
        maps.error = null
        ubicacionSeleccionada = ubicacion.first()
    }

    /**
     * Metodo que muestra la direccion de un marcador
     */
    override fun onMarkerDragEnd(marker: Marker) {
        var geocoder = Geocoder(this)
        var ubicacion = geocoder.getFromLocation(marker.position.latitude,marker.position.longitude,1)
        if(ubicacion.size > 0){
            maps.setText("${ubicacion.first().countryName}, ${ubicacion.first().adminArea}")
            maps.error = null
            ubicacionSeleccionada = ubicacion.first()
        }else{
            Toast.makeText(this," Ubicación invalida", Toast.LENGTH_LONG).show()
        }


    }


    /**
     * Metodo que carga el calendario para el campo de fecha
     */
    private fun cargarCalendario(): DatePickerDialog.OnDateSetListener {

        val fechaTxt: TextView  = findViewById(R.id.fechaOrientacion)

        var cal = Calendar.getInstance()

        fechaTxt.text = SimpleDateFormat("dd-MMMM-yyyy").format(System.currentTimeMillis())

        val dateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->


            val fecha =DateTime("$year-${monthOfYear+1}-$dayOfMonth")

            fechaSeleccionada = fecha

            fechaTxt.text = SimpleDateFormat("dd-MMMM-yyyy").format(fecha.toDate())


        }

        return dateSetListener

    }


    /**
     * Metodo que permite validar que los campos obligatorios sean llenados
     */
    private fun validarCamposVacios(): Boolean {
        var hasErrors = false
        if(maps.text.toString().isEmpty()){
            maps.error = ErroresEnum.CAMPO_VACIO.mensaje
            hasErrors = true

        }
        return  hasErrors
    }

    override fun onMarkerDragStart(p0: Marker) {
        p0.position
    }

    override fun onMarkerDrag(p0: Marker) {
        p0.position
    }





}
