package com.example.eduardo.helios.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.eduardo.helios.R
import com.example.eduardo.helios.enums.GraficasEnum
import com.example.eduardo.helios.enums.OperacionesEnum
import com.example.eduardo.helios.modelo.Panel
import com.example.eduardo.helios.modelo.SensadoPanel
import com.example.eduardo.helios.utileria.DateUtil
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import kotlinx.android.synthetic.main.fragment_monitorear_panel.*
import kotlinx.android.synthetic.main.fragment_monitorear_panel.view.*
import org.joda.time.DateTime
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.text.SimpleDateFormat
import java.util.*


class MonitorearPanel : Fragment(), Response.Listener<JSONObject>, Response.ErrorListener {

    private var ip = ""
    private var tipoGrafica = 0
    private var nombrePanel = ""
    private var nombrePeriodo = ""

    private var fechaInicio = ""
    private var fechaFin = ""


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_monitorear_panel, container, false)


        val fin = BufferedReader(
              InputStreamReader(activity!!.openFileInput("ip.txt")))
         ip = fin.readLine()
        fin.close()

        obtenerPaneles()




        val adapter = ArrayAdapter.createFromResource(activity,
                R.array.CTG_PERIODO, android.R.layout.simple_spinner_item)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        if (rootView.tipoPeriodo != null) {
            rootView.tipoPeriodo.adapter = adapter

            rootView.tipoPeriodo?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

                val now = DateTime.parse("2019-04-30")

                override fun onNothingSelected(parent: AdapterView<*>) {
                    nombrePeriodo = parent.selectedItem.toString()
                }

                override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                    tipoGrafica = position
                    nombrePeriodo = parent.getItemAtPosition(position).toString()
                    if (nombrePeriodo == "Dia") {
                        fechaInicio = "${now.year}-${now.monthOfYear}-${now.dayOfMonth}"
                        fechaFin = "${now.year}-${now.monthOfYear}-${now.dayOfMonth+1}"
                    } else if (nombrePeriodo == "Semana") {
                        fechaInicio = DateUtil.calcularFechaInicial()
                        fechaFin = DateUtil.calcularFechaFinal()
                    } else if (nombrePeriodo == "Mes") {
                        fechaInicio = "${now.year}-${now.monthOfYear}-1"
                        fechaFin = "${now.year}-${now.monthOfYear}-28"
                    }

                    val url = "http://$ip/monitoreoPanel.php?nombre=$nombrePanel" +
                            "&fechaInicio=$fechaInicio" +
                            "&fechaFin=$fechaFin"
                    val jrq = JsonObjectRequest(Request.Method.GET, url, null, this@MonitorearPanel, this@MonitorearPanel)
                    val rq = Volley.newRequestQueue(context)
                    rq.add(jrq)
                }

            }
        }

        if (rootView.panelSpinner != null) {
            rootView.panelSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

                override fun onNothingSelected(parent: AdapterView<*>) {
                    nombrePanel = parent.selectedItem.toString()
                }

                override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {


                    nombrePanel = parent.getItemAtPosition(position).toString()
                    val url = "http://$ip/monitoreoPanel.php?nombre=$nombrePanel" +
                            "&fechaInicio=$fechaInicio" +
                            "&fechaFin=$fechaFin"
                    val jrq = JsonObjectRequest(Request.Method.GET, url, null, this@MonitorearPanel, this@MonitorearPanel)
                    val rq = Volley.newRequestQueue(context)
                    rq.add(jrq)
                }

            }
        }

        return rootView
    }

    fun obtenerPaneles() {
        val url = "http://$ip/consultarHistorialPanel.php"
        val jrq = JsonObjectRequest(Request.Method.GET, url, null, this, this)
        val rq = Volley.newRequestQueue(context)
        rq.add(jrq)
    }

    override fun onResponse(response: JSONObject) {
        val listaPaneles: ArrayList<Panel> = ArrayList()
        val nombrePaneles: ArrayList<String> = ArrayList()

        val jsonArray: JSONArray = response.optJSONArray("datos")


        var longitud = jsonArray.length()

        var tipoRespuestaJson = jsonArray.getJSONObject(0)
        val tipoRespuesta = tipoRespuestaJson.optInt("operacion")


        if (tipoRespuesta == OperacionesEnum.CONSULTAR.identificador) {
            for (i in 1 until longitud) {

                var jsonObject = jsonArray.getJSONObject(i)

                var panel: Panel = Panel()

                panel.id = jsonObject.optString("id_panel").toInt()
                panel.nombre = jsonObject.optString("nombre")
                panel.voltajeMax = jsonObject.optString("voltaje_max").toDouble()
                panel.corrienteMax = jsonObject.optString("corriente_max").toDouble()

                listaPaneles.add(panel)
            }
            listaPaneles.forEach {
                nombrePaneles.add(it.nombre)
            }

            val adapter = ArrayAdapter(context, android.R.layout.simple_spinner_dropdown_item, nombrePaneles)


            panelSpinner.adapter = adapter
        } else if (tipoRespuesta == OperacionesEnum.MONITOREAR.identificador) {


            val listaSensadoPanel: ArrayList<SensadoPanel> = ArrayList()
            for (i in 1 until longitud) {
                var jsonObject = jsonArray.getJSONObject(i)

                var sensadoPanel = SensadoPanel()
                sensadoPanel.id = jsonObject.optInt("id_sensado_p")
                sensadoPanel.idPanel = jsonObject.optInt("id_panel")
                sensadoPanel.hora = SimpleDateFormat("yyyy-mm-dd HH:mm:ss").parse(jsonObject.optString("hora"))
                sensadoPanel.voltaje = jsonObject.optDouble("voltaje")

                listaSensadoPanel.add(sensadoPanel)
            }
            var entradas = ArrayList<Entry>()
            val etiquetas = ArrayList<String>()

            var label = ""

            listaSensadoPanel.forEach {

                if (tipoGrafica == GraficasEnum.DIA.identficiador) {
                    var hora = it.hora.hours
                    var minuto = it.hora.minutes
                    label = "Voltaje por día"
                    var tiempo = "$hora.$minuto"
                    entradas.add(Entry(tiempo.toFloat(), it.voltaje.toFloat()))
                    etiquetas.add(tiempo)
                } else if (tipoGrafica == GraficasEnum.SEMANA.identficiador) {
                    label = "Voltaje por semana"
                    var calendar = Calendar.getInstance()
                    calendar.time = it.hora
                    entradas.add(Entry(calendar.get(Calendar.DAY_OF_MONTH).toFloat(), it.voltaje.toFloat()))
                } else if (tipoGrafica == GraficasEnum.MES.identficiador) {
                    label = "Voltaje por mes"
                    var calendar = Calendar.getInstance()
                    calendar.time = it.hora
                    entradas.add(Entry(calendar.get(Calendar.DAY_OF_MONTH).toFloat(), it.voltaje.toFloat()))
                }
            }


            val datos = LineDataSet(entradas, label)
            datos.color = R.color.graficasColor
            val data = LineData(datos)


            val list = arrayListOf("a", "b", "c")

            grafica.data = data

        }
    }


    override fun onErrorResponse(error: VolleyError?) {
        Toast.makeText(activity, error.toString(), Toast.LENGTH_SHORT).show()
    }


    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        fun newInstance(): MonitorearPanel {
            return MonitorearPanel()
        }
    }
}