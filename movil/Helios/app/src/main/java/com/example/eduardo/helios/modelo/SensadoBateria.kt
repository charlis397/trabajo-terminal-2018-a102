package com.example.eduardo.helios.modelo

import java.util.*

data class SensadoBateria(var id:Int = 0, var idBateria:Int = 0, var hora: Date = Date(), var voltaje:Double=0.0,var corriente:Double=0.0,var temperatura:Double=0.0,var voltajePWM:Double=0.0,var estadoFase:Int=1)